/*

wasai 2018

wrccdc candy factory emergency alarm and stop button

*/

#include <Adafruit_NeoPixel.h> // used because FastLED was strangely unstable on Nanos I got

#define ALARM_PIN A1
#define LED_PIN 6
#define BUTTON_PIN 2
#define KEY_PIN 3
#define NUMPIXELS 4
#define MAXBRIGHTNESS 180 // globally dim down the brightness 

/*

   DO NOT EDIT AFTER THIS POINT!

*/
boolean keyturn = false;
boolean allstop = false;
long runtime = 0;
long clearbufftime = 0;

/* BEGIN LED SPECIFIC CODE !! */
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, LED_PIN, NEO_RGB + NEO_KHZ800);

void leds_do_startup() {
  //byte color = random(100, 250);
  Serial.print("!START255");
  for (byte i = 0; i < NUMPIXELS; i++) {
    byte color = random(100, 250);
    Serial.print(",");
    Serial.print(color);
    pixels.setPixelColor(i, pixels.Color(color, color, color));
    pixels.show();
    delay(600);
  }
  Serial.println();
}

void leds_set_pixel(byte led, char color) {
  led = led - 1; // offset
  boolean sss = false;
  switch (color) {
    case 'r':
    case 'R':
      pixels.setPixelColor(led, pixels.Color(MAXBRIGHTNESS, 0, 0));
      sss = true;
      break;
    case 'b':
    case 'B':
      pixels.setPixelColor(led, pixels.Color(0, 0, MAXBRIGHTNESS));
      sss = true;
      break;
    case 'g':
    case 'G':
      pixels.setPixelColor(led, pixels.Color(0, MAXBRIGHTNESS, 0));
      sss = true;
      break;
    case 'y':
    case 'Y':
      pixels.setPixelColor(led, pixels.Color(150, 150, 0));
      sss = true;
      break;
    case 'o':
    case 'O':
      pixels.setPixelColor(led, pixels.Color(0, 0, 0));
      sss = true;
      break;
    default:
      Serial.println("!INVALID");
      break;
  }
  if (sss) {
    pixels.show();
    Serial.print("!SET");
    Serial.print(led);
    Serial.print(",");
    Serial.println(color);
  }
}
void leds_all_off() {
  for (byte i = 0 ; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));
  }
  pixels.show();
}

void leds_all_red() {
  for (byte i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(250, 0, 0));
    pixels.show();
  }
}

void leds_all_green() {
  for (byte i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(0, 250, 0));
  }
  pixels.show();
}

/* END LED SPECIFIC CODE !! */

void process_serial(char fc, char sc) {
     /*
       character 1:
        1-4 = LED selection
        R = reset selection
        B = Trigger Alarm

        character 2:
        R,G,B (LED selection is color)
        for bell: 0-9 is duration
    */
  if (fc == 'R' or fc == 'r') {
    // do a reset!
    allstop = false;
    leds_all_off();
    leds_all_green();
    tone(ALARM_PIN, 200, 400);
    delay(400);
    tone(ALARM_PIN, 800, 400);
    leds_all_off();
    Serial.println("!READY");
  } else if (fc == 'B' or fc == 'b') {
    // bell time!
    // do an alarm for 30 seconds!at
    int bell_duration_val = (byte)(String(sc).toInt()); // ugly but it works
    long sf = 0;
    if (bell_duration_val > 0) {
      sf = bell_duration_val * 10000;
    } else {
      sf = 30000;
    }
    Serial.print("!BELL");
    Serial.println(sf);
    tone(ALARM_PIN, 1000, sf);
  }
  // if it doesn't match any of these. we might have a color!
  int led_val_int = String(fc).toInt(); // ugly but it works
  byte led_val = (byte)led_val_int;
  if (led_val > 0 && led_val <= NUMPIXELS) {
    leds_set_pixel(led_val, sc);
  }
}

void setup() {
  Serial.begin(9600);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(KEY_PIN, INPUT_PULLUP);
  //pinMode(LED_BUILTIN, OUTPUT);
  
  pixels.begin(); // This initializes the NeoPixel library.
  leds_all_off();
  leds_do_startup();
  delay(1000);
  leds_all_off();
  Serial.println("!READY");
  //digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  runtime = millis();
  byte button_input = digitalRead(BUTTON_PIN);
  byte key_input = digitalRead(KEY_PIN);
  if (key_input == HIGH) {
    if (!allstop) {
      keyturn = true;
      leds_all_red();
      tone(ALARM_PIN, 500, 1000);
      if (button_input == HIGH) {
        for(byte i=0; i<5; i++){
          Serial.println("!ALLSTOP");
          delay(100);
        }
        allstop = true;
        boolean flop = false;
        for (byte i = 0; i < 10; i++) {
          tone(ALARM_PIN, 300, 500);
          if (flop) {
            leds_all_off();
            flop = false;
          } else {
            leds_all_red();
            flop = true;
          }
          delay(1000);
        }
      }
    } // allstop not triggered
  } else {
    if (keyturn) {
      leds_all_off();
      keyturn = false;
    }
  }

  // now process data from serial
  if (Serial.available() > 1) {
    // we should have two characters
    char fs = (char)Serial.read();
    char ss = (char)Serial.read();
    process_serial(fs, ss);
  } else {
    if (runtime - clearbufftime > 500) {
      // we now clear our buffer
      while (Serial.available() > 0) {
        Serial.read();
      }
    } else {
      clearbufftime = millis();
    }
  }
  delay(500);
}
