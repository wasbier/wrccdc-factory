#!/bin/bash
cd /opt/controller/enabled || exit
systemctl disable xinetd
systemctl stop xinetd
killall /usr/bin/python3
killall monitor.py
for i in *;
do
	echo $i
	cat > /etc/systemd/system/${i}-controller.service <<END
[Unit]
Description=${i} Controller
Wants=network.target
After=network.target

[Service]
Type=simple
PIDFile=/tmp/${i}-controller.pid
ExecStart=/opt/controller/enabled/${i} -c /opt/controller/config.yml
ExecStop=/bin/kill -s TERM \$MAINPID
Restart=on-failure

[Install]
WantedBy=multi-user.target
	
END
done
systemctl daemon-reload
for i in *;
do
systemctl enable ${i}-controller.service
systemctl restart ${i}-controller
done
cat > /etc/systemd/system/monitor-controller.service <<END
[Unit]
Description=monitor Controller
Wants=network.target
After=network.target

[Service]
Type=forking
PIDFile=/tmp/monitor-controller.pid
ExecStart=/opt/controller/monitor.py -c /opt/controller/config.yml -p /tmp/monitor-controller.pid
ExecStop=/bin/kill -s TERM \$MAINPID
Restart=on-failure

[Install]
WantedBy=multi-user.target
	
END
systemctl daemon-reload
systemctl enable monitor-controller.service
systemctl restart monitor-controller