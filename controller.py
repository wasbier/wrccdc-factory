#!/usr/bin/python3
import yaml, json
import sys, os, random
import syslog, argparse
import socket, threading, socketserver
from copy import deepcopy
from daemonize import Daemonize
from pprint import pprint

CONTROLLER=""
METADATA={}
DEBUG=True # normally false
CFG=None
FILEWATCH = {} # store our mtimes in here.
CRITICAL=False
RUN=True

### IF YOU WANT TO HANDLE ALLL EXCEPTIONS

def handle_exception(type,value,traceback):
	debug("error: %s with error '%s' on line %s of %s" % (str(type),str(value),str(traceback.tb_lineno),str(traceback.tb_frame.f_code.co_filename)))
#sys.excepthook = handle_exception

def debug(msg):
	global CFG,DEBUG
	if DEBUG:
		if 'xinetd' in CFG and CFG['xinetd'] is False:
			print(msg)
		#syslog.syslog(msg)


class HandleTCPConnection(socketserver.BaseRequestHandler):
	def handle(self):
		if os.path.exists("/tmp/controller-%s.fault" % CONTROLLER):
			return
		debug("info: got connection from %s" % str(self.client_address[0]))
		self.data = self.request.recv(1024).strip()
		recv = self.data
		send = handle_connection(recv.decode())
		self.request.sendall(send.encode())

    
def update_values(input):
	global METADATA
	if input is not None and input != '':
		i = None
		try:
			debug("info: data was detected as json and decoded")
			i = json.loads(input)
		except:
			print("")
			try:
				debug("info: data was detected as yaml and decoded")
				i = yaml.load(str(input).lstrip('\n'))
			except:
				debug("error: data was of an unknown type and ignored")
				return False
		if i is not None:
			# take exactly what we see, or interp. 
			if 'data' in i:
				debug("info: detected ng format for data input")
				d = i['data']
			else:
				debug("info: detected og format for data input")
				d = i
			for k,v in d.items():
				if isinstance(v,dict):
					for component,value in v.items():
						if component not in ('critical','type','value'):
							debug(" '%s' => '%s' on '%s'" % (component,str(value),k))
							METADATA[k][component] = value 
				else:
					debug("error: unable to restore metadata %s" % (k))
	return True
	
def load_metadata():
	global CFG, METADATA
	appname="controller-%s" % (str(CONTROLLER).lower())
	apppath = CFG['tmp_state_dir'].rstrip('/') + "/" + appname + ".state"
	if os.path.exists(apppath):
		with open(apppath,'r') as md:
			debug("info: metadata has been loaded from %s" % apppath)
			METADATA = yaml.load(md)
			md.close()
	else:
		debug("error: unable to load metadata, resetting. ")

def store_metadata():
	global CFG, METADATA
	debug("attempting to store metadata")
	appname="controller-%s" % (str(CONTROLLER).lower())
	apppath = CFG['tmp_state_dir'].rstrip('/') + "/" + appname + ".state"
	with open(apppath, 'w+') as md:
		debug("storing metadata to %s" % apppath)
		yaml.dump(METADATA, md, default_flow_style=False)
		md.close()

def check_critical():
	global CFG, METADATA, CONTROLLER
	appname="controller-%s" % (str(CONTROLLER).lower())
	critical_check = CFG['tmp_state_dir'].rstrip('/') + "/" + appname + ".fault"
	if os.path.exists(critical_check):
		return True
	else:
		if 'value' in METADATA and METADATA['value'] >= METADATA['critical']:
			open(critical_check, 'a').close()
			return True
	return False

	
def range_stepper(value,step,target,direction='increment'):
	global CRITICAL
	prob_add = 10
	if direction == 'increment':
		prob_add = 75
		# give us some randomness until we catch
		if target > value * 10:
			prob_add = 95
		if value < 10:
			debug("warning: (increment) totalizer (%d) is too low, initialization occuring" % value)
			return random.randrange(step,30)
		elif value > target:
			debug("warning: (increment) totalizer (%d) is is above target range (%d), throttling down" % (value,target))
			prob_add = 5
	elif direction =="refill":
		v = value-random.randrange(0,2)
		if value <= 0:
			debug("warning: (refill) totalizer is too low, initialization occuring")
			return target
		else:
			return v
	elif direction == 'decrement':
		prob_add = 25
		step = random.randrange(0,step)

	possible_range = 0
	max_loop = 1000
	while (max_loop>0):
		max_loop=max_loop-1 
		if random.randint(0,100) < prob_add:
			possible_range = value+step
		else:
			possible_range = value-step
		
		if direction == "refill":
			possible_range=value-1
			break;
		#print("attempting to find a value between %d and %d with a step of %d" % (value,possible_range,step),end="")
		try:
			if value == 0 and possible_range == 0:
				return 1
			# % probability	
			if possible_range < value:
				value = random.randrange(possible_range,value)
			else:
				value = random.randrange(value,possible_range)
			return value
		except ValueError:
			debug("totalizer error, resetting totalizer to 0")
			return 0
	# give up? lol
	return value

def run_totalizer():
	global CFG, CONTROLLER, CRITICAL
	node = CFG[CONTROLLER]
	
	# start our data if it isn't set.
	for control,limits in node['controls'].items():
		# do a populate?????
		if control not in METADATA:
			debug(str(list(METADATA)))
			debug("initializing %s to metadata for controller %s " % (control,CONTROLLER))
			METADATA[control] = {}
			METADATA[control]['type'] = CFG[CONTROLLER]['type']
			METADATA[control]['active'] = True
			METADATA[control]['value'] = 0
			for limit_type,limit_value in limits.items():
				if limit_type not in METADATA[control]:
					METADATA[control][limit_type] = limit_value
		
		# there once was some code here to check if value was set > 0. oh well
		
		# if our state is now "off"	or we've gone critical
		if ('active' in METADATA and METADATA[control]['active'] is False) or CRITICAL:
			METADATA[control]['active'] = False;
			debug("warning: controller has halted")
			if METADATA[control]['type'] is 'increment':
				value = random.randrange(abs(value-5),value)
			else:
				value = value-1	
			# 0 out our value?
			if value <= 1:
				value = 0
		else:	
			# now set our VALUES
			target = METADATA[control]['target']
			cur_value = int(METADATA[control]['value'])
			if METADATA[control]['type'] in ['decrement', 'increment', 'refill']:
				value = range_stepper(cur_value,CFG['step'],target,METADATA[control]['type'])
			METADATA[control]['value'] = value
	
def handle_connection(input=None):
	global CFG, CONTROLLER, METADATA, CRITICAL
	node_conf = CFG[CONTROLLER]
	#load_metadata()
	run_totalizer()
	CRITICAL = check_critical()
	m = {}
	m['info'] = {}
	m['data'] = deepcopy(METADATA)
	# purge the copies of critical
	for col in list(m['data'].keys()):
		m['data'][col].pop('type',None)
		
	m['info']['unitid'] = CFG[CONTROLLER]['unitid']
	m['info']['pretty_name'] = CFG[CONTROLLER]['pretty_name']
	m['info']['controller'] = CONTROLLER
	
	if node_conf['protocol'] == 'json':
		output = json.dumps(m,sort_keys=True,indent=4, separators=(',', ': '))
	elif node_conf['protocol'] == 'yaml':
		output = yaml.dump(m)
	else:
		debug("error: message protocol type not supported, bailing")
		sys.exit(1)
	
	# now we try to decode the data (blindly)
	update_values(input)
	# now store metadata state
	store_metadata()
	debug("completed operational cycle")
	return output

def start():
	global CFG, CONTROLLER
	node_conf = CFG[CONTROLLER]
	load_metadata()
	run_totalizer()
	print(node_conf)
	if 'ip' not in node_conf or CFG['xinetd']:
		while RUN:
			data = sys.stdin.readline().strip()
			sys.stdout.flush()
			handle_connection(data)
	else:
		server = socketserver.TCPServer((node_conf['ip'], int(node_conf['port'])), HandleTCPConnection)
		server.serve_forever()
	
if __name__ == "__main__":
	# provide us with the option of not asking for device if the controller is linked
	controller_basename = os.path.basename(__file__).replace(".py","").replace(".pyc","")
	arg_opt = True # normally true
	if "controller" not in controller_basename:
		arg_opt = False

	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--config', help='set the configuration definitions file to this file',required=True)
	parser.add_argument('-v', '--verbose', action='store_true', help='turn on debugging')
	parser.add_argument('-x', '--xinetd', action='store_true', help='force xinetd support instead of socket')
	parser.add_argument('-n', '--nofork', action='store_true', help='do not run as a daemon')
	parser.add_argument('-d', '--device', help='specify the type of controller to be used.',required=arg_opt)
	args = parser.parse_args()
	config_file = str(args.config)
	
	if not os.path.exists(config_file):
		debug("error: please provide a valid configuration file")
		sys.exit(1)
	if args.verbose is not None:
		DEBUG=True
	if args.device is not None:
		CONTROLLER=str(args.device)
	else:
		CONTROLLER=controller_basename

	# yaml examples recommend using with...
	with open(config_file,'r') as stream:
		cfg_data = yaml.load(stream)
	
	if CONTROLLER not in cfg_data:
		debug("error: controller (%s) not detected, bailing" % CONTROLLER)
		sys.exit(1)
	CFG=cfg_data
	
	if args.xinetd is not None and True == False:
		debug("info: controller %s is starting from config '%s' as xinetd instance" % (CONTROLLER,config_file))
		CFG['xinetd'] = True	
		start()
	else:
		CFG['xinetd'] = False	
	if args.nofork is None:
		pid = cfg_data['pidfile']
		debug("info: controller %s is starting from config '%s' as daemon instance" % (CONTROLLER,config_file))
		appname="controller-%s" % (str(CONTROLLER).lower())
		daemon = Daemonize(app=appname, pid=pid, action=start)
		daemon.start()
	else:
		debug("info: controller %s is starting from config '%s' as foreground instance" % (CONTROLLER,config_file))
		start()

# vim: set noexpandtab :