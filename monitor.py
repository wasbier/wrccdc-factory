#!/usr/bin/python3
import yaml, json, serial
import sys, os, random, time
import syslog, argparse
import socket, threading, socketserver
from copy import deepcopy
from daemonize import Daemonize
from pprint import pprint

METADATA={}
CONFIG="/opt/controller/config.yml"
DEBUG=True # normally false
CFG=None
FILEWATCH = {} # store our mtimes in here.
CRITICAL=False
ALARM=False
RUN=True

def debug(msg):
	global CFG,DEBUG
	if DEBUG:
		print(msg)
		syslog.syslog(msg)

def check_critical(controller):
	global CFG, METADATA
	appname="controller-%s" % (str(controller).lower())
	critical_check = CFG['tmp_state_dir'].rstrip('/') + "/" + appname + ".fault"
	if os.path.exists(critical_check):
		return True
	else:
		if 'value' in METADATA and METADATA['value'] >= METADATA['critical']:
			open(critical_check, 'a').close()
			ALARM=True
			return True
	return False

def check_each_critical():
	for k,v in METADATA.items():
		critical = check_critical(k)
		if critical:
			return True
	return False
		
def load_config(apppath):
	global CFG, METADATA
	if os.path.exists(apppath):
		with open(apppath,'r') as md:
			debug("info: config has been loaded from %s " % (apppath))
			CFG = yaml.load(md)
			md.close()
			# now copy.
			exclude = ('step','sync-delay','version','run_dir','tmp_state_dir','perm_state_dir','enabled_device_dir','serial_port','serial_baud')
			for k,v in CFG.items():
				if k not in exclude:
					METADATA[k]=deepcopy(CFG[k]['controls'])
					#del(CFG[k])
	else:
		debug("error: unable to load config")

def load_metadata_file(controller):
	global CFG, METADATA
	appname="controller-%s" % (str(controller).lower())
	apppath = CFG['tmp_state_dir'].rstrip('/') + "/" + appname + ".state"
	if os.path.exists(apppath):
		with open(apppath,'r') as md:
			debug("info: metadata has been loaded from %s for controller %s" % (apppath,controller))
			METADATA[controller] = yaml.load(md)
			md.close()
			return True
	else:
		debug("error: unable to load metadata file for controller %s" % controller)
		return False
	
def read_metadata_query(controller):
	global CFG, METADATA
	if not controller in CFG:
		return False
	HOST=CFG[controller]['ip']
	PORT=CFG[controller]['port']
	debug("info: attempting to load metadata from %s from %s:%d" % (controller,HOST,PORT))
	s = None
	for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
		af, socktype, proto, canonname, sa = res
		try:
			s = socket.socket(af, socktype, proto)
		except OSError as msg:
			s = None
			continue
		try:
			s.connect(sa)
		except OSError as msg:
			s.close()
			s = None
			continue
		break
	if s is None:
		return None
	with s:
		s.sendall(b'\r\n\r\n')
		data = s.recv(1024)
		processed_data = data.decode('utf-8')
		try:
			j = json.loads(processed_data)
			METADATA[controller] = deepcopy(j['data'])
			return True
		except json.decoder.JSONDecodeError:
			return False
		s.close()

def check_metadata():
	global CFG,METADATA,ALARM
	probabilities = {}
	for controller,component in METADATA.items():
		worst_prob = 0.0
		for comp_name,v in component.items():
			real_prob = 0.0
			# check how we're observing health. 
			#if v['target']<v['critical'] or "refill" in CFG[controller]['type'] or CFG[controller]['type']=="decrement":
			#max=v['target']
			#else:
			# do we have stuff thats active?
			try:
				if "active" in v and v['active']:
					if "refill" in CFG[controller]['type'] or "decrement" in CFG[controller]['type']:
						real_prob = 0.5
					else:
						if int(v["critical"]) <= 0:
							real_prob = 0.5
						real_prob = float(v['value'])/float(v['critical']) ##THESE ARE THE SAME DAMN THING?
				else:
					real_prob = 0.5
				debug("info: got value %d/%d=%f (real %s) with type %s on %s in controller %s" % (v['value'],v['critical'],worst_prob,real_prob,CFG[controller]['type'],comp_name,controller))
			except:
				real_prob = 0.5
			if real_prob > worst_prob:
				worst_prob = real_prob
		# trigger alarm
		if abs(worst_prob)>.95:
			ALARM=True
		probabilities[controller] = abs(worst_prob)
	return probabilities

def load_metadata(controller=None):
	global METADATA
	if controller is not None:
		if not load_metadata_file(controller):
			read_metadata_query(controller)		
	for k,v in METADATA.items():
		if not load_metadata_file(k):
			read_metadata_query(k)
	#pprint(METADATA)

def send_all_stop():
	global CFG, METADATA
	for controller,v in METADATA.items():
		appname="controller-%s" % (str(controller).lower())
		critical_check = CFG['tmp_state_dir'].rstrip('/') + "/" + appname + ".fault"
		open(critical_check, 'a').close()
	
def main():
	global METADATA, CONFIG, CFG, RUN, ALARM
	debug("info: loading configuration and metadata")
	load_config(CONFIG)
	# now we can do stuff!
	ser = None
	shutoff_time = 0
	try:
		ser = serial.Serial(CFG['serial_port'], CFG['serial_baud'], timeout=1) 
	except:
		debug("error: unable to load serial connection")
	ser_tries = 5 # reties remaining
	while RUN:
		#time.sleep(int(CFG['sync-delay'])) # cool.
		critical = check_each_critical()
		load_metadata() # refresh! 
		percentage_use = check_metadata()
		if ser is None or not ser.is_open:
			if ser_tries>0:
				ser_tries=ser_tries-1
				try:
					ser.close()
					ser = serial.Serial(CFG['serial_port'], CFG['serial_baud'], timeout=1) 
				except:
					debug("error: unable to load serial connection, %d tries remaining" % ser_tries)
		else:
			# we have serial. lets see if we have data?
			ser.setDTR(1)
			message = ser.readline()
			pprint(message)
			pprint(ser.inWaiting())
			allstop=False
			if allstop or "allstop" in str(message).lower():
				debug("info: ALL STOP TRIGGERED!")
				allstop=True
				if (ALARM and (time.time()-shutoff_time)<60) or shutoff_time == 0:
					send_all_stop() # they've safely shutdown. 
					ser.write(b'%dR\n' % 0)
					time.sleep(1)
					ser.write(b'%dR\n' % 1)
					time.sleep(1)
					ser.write(b'%dR\n' % 2)
					time.sleep(1)
					ser.write(b'%dR\n' % 3)
				else:
					debug("The shutoff time was %s and now is %s" % (shutoff_time, time.time()))
					debug("error: CRITICAL SHUTDOWN DID NOT HAPPEN IN TIME")
					send_all_stop()
					ser.write(b'%dR\n' % 0)
					time.sleep(1)
					ser.write(b'%dR\n' % 1)
					time.sleep(1)
					ser.write(b'%dR\n' % 2)
					time.sleep(1)
					ser.write(b'%dR\n' % 3)
			elif shutoff_time != 0 and shutoff_time + 60 < time.time():
				debug("error: CRITICAL SHUTDOWN DID NOT HAPPEN IN TIME")
				allstop=True
				send_all_stop()
				ser.write(b'%dR\n' % 0)
				time.sleep(1)
				ser.write(b'%dR\n' % 1)
				time.sleep(1)
				ser.write(b'%dR\n' % 2)
				time.sleep(1)
				ser.write(b'%dR\n' % 3)
			while allstop:		
				# now check to see if our files are gone. otherwise. we don't do anything 
				if not check_each_critical():
					allstop=False
					ALARM=False
					debug("info: ALL CLEAR SIGNALED")
					ser.write(b'R2\n')	

			# do some checks for normal stuff
			if ALARM and shutoff_time == 0:
				debug("info: ALARM HAS BEEN TRIGGERED!")
				shutoff_time = time.time()
				ser.write(b'B6\n') # 60 second bell! 
			elif not ALARM:
				debug("info: ALARM AT SAFE LEVELS")
				shutoff_time=0
			i=1
			for component,percent in percentage_use.items():
				if percent<=.75 and percent>0: # its good
					debug("info: Green (%f) set for %d light" % (percent,i))
					ser.write(b'%dg\n' % i) 
				elif percent>.75 and percent<=.90: # its not super good
					debug("info: Yellow (%f) set for %d light" % (percent,i))
					ser.write(b'%dy\n' % i)	
				elif percent>.90: # its critical
					debug("info: Red (%f) set for %d light" % (percent,i))
					ser.write(b'%db\n' % i)
				elif percent<=0.0: # its off
					debug("info: Off (%f) set for %d light" % (percent,i))
					ser.write(b'%do\n' % i)
				else:
					debug("info: Not Processing (%f) set for %d light" % (percent,i))
				message = ser.readline()
				if "allstop" in str(message).lower():
					allstop=True
				time.sleep(2)
				i=i+1	

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--config', help='set the configuration definitions file to this file',required=True)
	parser.add_argument('-p', '--pid', help='set the pid file location',required=False)
	parser.add_argument('-v', '--verbose', action='store_true', help='turn on debugging')
	parser.add_argument('-n', '--nofork', action='store_true', help='do not run as a daemon')
	args = parser.parse_args()
	config_file = str(args.config)
	
	if not os.path.exists(config_file):
		debug("error: please provide a valid configuration file")
		sys.exit(1)
	if args.verbose is not None:
		DEBUG=True

	# yaml examples recommend using with...
	with open(config_file,'r') as stream:
		cfg_data = yaml.load(stream)
	CFG=cfg_data
	
	pprint(args.nofork)
	if args.nofork is None or args.nofork is False:
		if args.pid is None:
			pidfile="/var/run/controller-monitor.pid"
		else:
			pidfile=args.pid
		debug("info: monitoring system started from config '%s' as daemon instance" % (config_file))
		appname="monitor"
		daemon = Daemonize(app=appname, pid=pidfile, action=main)
		daemon.start()
	else:
		debug("info: monitoring system started from config '%s' as foreground instance" % (config_file))
		main()
