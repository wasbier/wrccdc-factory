#!/usr/bin/python3
import yaml, json
import sys, os, random
import syslog, argparse
from pprint import pprint

xinetd_conf_file = "/etc/xinetd.d/controller"

USER = "root"
PORT = 502 # modbus

def config_block(name,ip,port,user,server,args):
	return """
service %s
{
    protocol       = tcp
    disable        = no
    type           = UNLISTED
    port           = %d
    flags          = REUSE
    socket_type    = stream
    wait           = no
    user           = %s
    server         = %s
    server_args    = %s 
    bind           = %s
    log_on_failure += USERID
}
""" % (name,port,user,server,args,ip)



if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--config', help='set the configuration definitions file to this file',required=True)
	args = parser.parse_args()
	config_file = str(args.config)
	
	# make sure we have a valid full path config file for use
	if not os.path.exists(config_file):
		print("error: please provide a valid configuration file")
		sys.exit(1)
	if not os.path.isabs(config_file) or config_file[0] is not "/":
		print("error: please provide absolute path for configuration!")
		sys.exit(1)
	
	# try to create the file if it does not exist
	# or empty it 
	try:
		c = open(xinetd_conf_file,'w+')
		c.write("# automatically generated config file")
		c.write("\n")
		c.close()
		print("info: file %s initialized" % xinetd_conf_file)
	except:
		print("error: unable to perform file creation and init")
		print("question: do we have permission and does the dir exist?")
		sys.exit(1)
		pass
	# now append the file for good.
	with open(xinetd_conf_file,'a') as xinetd_conf:
		with open(config_file,'r') as stream:
			cfg_data = yaml.load(stream)
			cfg_data.pop('step',None)
			cfg_data.pop('version',None)
			for controller,configuration in cfg_data.items():
				# run only if we have a boiler and not a general configuration
				if isinstance(configuration, dict) and "_dir" not in controller:
					ip = configuration['ip']
					alt_port = configuration['port']
					src = str(cfg_data['run_dir']).rstrip('/') + "/controller.py"
					dest = str(cfg_data['enabled_device_dir']).rstrip('/') + "/" + controller
					args = "-xc %s" % config_file
					if os.path.islink(dest):
						print("info: %s aleady exists, skipping" % dest)
					else:
						print("info: %s does not exist, creating" % dest)
						os.symlink(src,dest)
					# now that we're done making links, lets create the configs
					try:
						appending_content = config_block(controller.strip(),ip,PORT,USER,dest,args)
						print("info: added controller %s on %s:%d" % (dest,ip,PORT))
						xinetd_conf.write(appending_content)
					except:
						print("error: unable to add controller %s on %s:%d" % (controller,ip,PORT))
					#print(appending_content)
				else:
					continue
			stream.close()
		xinetd_conf.close()
print("info: completed")
